"""
fileIO handle file IO stuff
"""

import os
import path
import sys
import numpy as np
from paramiko import SSHClient
from paramiko import transport
from scp import SCPClient


def get_node_ip(src_dir):
    """
    read nodes ips from file
    """
    IP_fn = os.path.join(src_dir, "projects",
                         "cluster_conf", "conf", "nodes_ip.txt")
    IP_vec = np.loadtxt(IP_fn, dtype=str, delimiter="\n")
    return IP_vec


def get_node_hostname(src_dir):
    """
    read nodes hostnames  from file
    """
    nodes_name_fn = os.path.join(src_dir, "projects",
                                 "cluster_conf", "conf", "nodes_name.txt")
    hostname_vec = np.loadtxt(nodes_name_fn, dtype=str, delimiter="\n")
    return hostname_vec


def getHostname2IP(src_dir):
    """
    return a dictionary of hostname to IP
    """
    h2IP = {}
    IP_v = get_node_ip(src_dir)
    hostname_v = get_node_hostname(src_dir)
    if len(IP_v) != len(hostname_v):
        raise ValueError("IP vector has different length as hostname vector")
    for idx, ip in enumerate(IP_v):
        hostname = hostname_v[idx]
        h2IP[str(hostname)] = str(ip)
    return h2IP


def progress(filename, size, sent, peername):
    """
    # you can also add 4th parameter to track IP and port
    # useful with multiple threads to track source
    """
    sys.stdout.write("(%s:%s) %s\'s progress: %.2f%%   \r" % (
        peername[0], peername[1], filename, float(sent)/float(size)*100))


def scpFromIP(IP, remote_dir, remote_filename, local_dir, local_filename):
    """
    https://pypi.org/project/scp/
    """
    ssh = SSHClient()
    # host_key_filename = os.path.join(
    #     os.path.expanduser("~"), ".ssh", "known_hosts")
    # ssh.load_system_host_keys(host_key_filename)
    ssh.load_system_host_keys()
    ssh.connect(str(IP), username="Invictus", password=None)

    print("\nFile transfer started...")
    if str(remote_dir).endswith("/"):
        remote_path = remote_dir + remote_filename
    else:
        remote_path = remote_dir + "/" + remote_filename

    # SCPCLient takes a paramiko transport and progress callback as its arguments.
    with SCPClient(ssh.get_transport(), progress=progress) as scp:
        scp.get(remote_path=remote_path, local_path=os.path.join(
            local_dir, local_filename))

    print("\nFile transfer finished...\n")

    return


def scpFromHostname(hostname2IP, hostname, remote_dir, remote_filename, local_dir, local_filename):
    """
    scp from hostname, 
    """
    if hostname not in hostname2IP:
        raise ValueError("Hostname {0} not found in dictionary hostname2IP {1}".format(
            hostname, hostname2IP))
    ip = hostname2IP[hostname]
    # scp
    scpFromIP(ip, remote_dir, remote_filename, local_dir, local_filename)


def scpFromClusterAndMergeFile(hostname2IP, remote_dir, remote_filename, local_dir, hostnames=None):
    """
    scp from all nodes from hostname2IP dictionary and merge to one single file with the same name
    remote_filename will be used as filename of merged BIG local file
    """
    hostname_set = set(hostnames)
    if hostnames is not None and len(hostnames) >= 1:
        hostname_set.update(hostnames)
    local_merged_filename = os.path.join(local_dir, remote_filename)

    if os.path.exists(local_merged_filename):
        raise FileExistsError(
            "File {} already exist, please delete it manually and rerun".format(local_merged_filename))

    tmp_fn = remote_filename.split('.')[0] + \
        "_crap_ladfjasdfhadsf389fladsjf.txt"
    tmp_full_fn = os.path.join(local_dir, tmp_fn)
    with open(local_merged_filename, 'w') as merged_fh:
        for hostname, ip in hostname2IP.items():
            if hostname not in hostname_set:
                continue
            scpFromIP(ip, remote_dir, remote_filename, local_dir, tmp_fn)
            with open(tmp_full_fn, 'r') as tmp_fh:
                merged_fh.write(tmp_fh.read())
            print("File {0} from {1} of ip {2} shipped and merged to {3}".format(
                remote_filename, hostname, ip, local_merged_filename))
    if os.path.exists(tmp_full_fn):
        os.remove(tmp_full_fn)

    return


def shipFiles(src_dir, data_dir):
    """
    scp and merged some files, define your own shipFiles
    """
    hostname2IP = getHostname2IP(src_dir=SRC_DIR)
    remote_dir = "/home/Invictus/Documents/spring_2018_boulder/SRC_EXE_DIR/SOHR_DATA/output/"
    hostnames = ["headnode", "node1", "node2", "node3", "node4", "node5"]

    fn1 = "ign_global.csv"
    scpFromClusterAndMergeFile(hostname2IP,
                               remote_dir,
                               fn1,
                               local_dir=os.path.join(data_dir, "output"),
                               hostnames=hostnames)
    fn2 = "k_global.csv"
    scpFromClusterAndMergeFile(hostname2IP,
                               remote_dir,
                               fn2,
                               local_dir=os.path.join(data_dir, "output"),
                               hostnames=hostnames)
    fn3 = "uncertainties_range_backup.inp"
    scpFromHostname(hostname2IP,
                    "headnode",
                    "/home/Invictus/Documents/spring_2018_boulder/SRC_EXE_DIR/SOHR_DATA/input/",
                    fn3,
                    os.path.join(data_dir, "input"),
                    "uncertainties_range.inp")


if __name__ == '__main__':
    SRC_DIR = os.path.abspath(os.path.join(os.path.realpath(
        sys.argv[0]), os.pardir, os.pardir, os.pardir))
    DATA_DIR = os.path.abspath(os.path.join(os.path.realpath(
        sys.argv[0]), os.pardir, os.pardir, os.pardir, os.pardir, "SOHR_DATA"))
    print(SRC_DIR, DATA_DIR)
    shipFiles(SRC_DIR, DATA_DIR)
