"""
my utility
"""
import os
import sys
import numpy as np
import copy
import re
import time
import argparse
import parse_spe_reaction_info as psri


def read_duplicated_reactions(data_dir, fn):
    """
    read duplicated reactions
    """
    full_path2 = os.path.join(data_dir, "input", fn)
    if not os.path.exists(full_path2):
        raise FileNotFoundError(
            "File {0} not found, please run jobType {1} in C++ kernel".format(full_path2,
                                                                              "INITIATION"))

    dup_rxn_tmp = np.loadtxt(full_path2, dtype=int,
                             delimiter=',', comments='#')
    if len(np.shape(dup_rxn_tmp)) == 2:
        return dup_rxn_tmp[:, 1]
    else:
        return dup_rxn_tmp


def read_include_exclude_list(data_dir, fn="uncertainties_range.inp",
                              fn2="duplicated_reaction_index.csv"):
    """
    read exclude and include species index from input/uncertainties.inp, if uncertainties == 1.0,
    put it into exclude list
    """
    u_const = np.loadtxt(os.path.join(data_dir, "input", fn),
                         dtype=float, delimiter=',', comments='#')
    exc_list = []
    inc_list = []
    for idx, val in enumerate(u_const):
        if len(val) != 3:
            raise ValueError(
                "Length of uncertainties range file is {0} not 3 at line {1}".format(len(val), idx))
        if float(val[1]) == 0.0 or float(val[1] == float(val[2])):
            exc_list.append(int(val[0])-1)
        else:
            inc_list.append(int(val[0])-1)

    inc_set = set(inc_list)
    exc_set = set(exc_list)
    dup_rxn = read_duplicated_reactions(data_dir, fn2)

    # exclude duplicated reactions
    for _, val in enumerate(dup_rxn):
        r_idx = int(val)
        assert(r_idx >= 0)
        if r_idx in inc_set:
            inc_set.remove(r_idx)
        if r_idx not in exc_set:
            exc_set.add(r_idx)

    exc_list = list(exc_set)
    inc_list = list(inc_set)

    exc_list = list(sorted(exc_list))
    inc_list = list(sorted(inc_list))

    return inc_list, exc_list


def update_include_exclude_list(inc_list, exc_list, must_inc=None, inc_at_least_num=1,
                                inc_at_most_num=50):
    """
    update inc_list and exc_list according to must_inc-->must include index from this list
    if  inc_at_least_num--> include at lease this number of index
    """
    inc_set_base = set(copy.deepcopy(inc_list))

    inc_set = set(copy.deepcopy(inc_list))
    exc_set = set(copy.deepcopy(exc_list))

    # empty include list
    while len(inc_set) > 0:
        r_idx = inc_set.pop()
        exc_set.add(r_idx)

    if must_inc is not None:
        for _, val in enumerate(must_inc):
            r_idx = int(val)
            assert(r_idx >= 0)
            # make sure the index is a good index
            if r_idx not in inc_set and r_idx in inc_set_base:
                if r_idx in exc_set:
                    inc_set.add(r_idx)
                    exc_set.remove(r_idx)
    # if include start with a large size, match the initial size
    while (len(inc_set) < inc_at_least_num or len(inc_set) < len(inc_set_base)) \
            and len(exc_set) > 0:
        r_idx = exc_set.pop()
        # make sure it is a good index
        if r_idx in inc_set_base:
            inc_set.add(r_idx)
        else:
            exc_set.add(r_idx)
    while len(inc_set) > inc_at_most_num and len(inc_set) > 0:
        r_idx = inc_set.pop()
        exc_set.add(r_idx)

    inc_list2 = list(inc_set)
    exc_list2 = list(exc_set)

    inc_list2 = list(sorted(inc_list2))
    exc_list2 = list(sorted(exc_list2))

    print("Input size include list: {0}, exclude list: {1}".format(
        len(inc_list), len(exc_list)))
    print("Output size include list: {0}, exclude list: {1}".format(
        len(inc_list2), len(exc_list2)))
    print("Output include list:\n{0}".format(inc_list2))
    return inc_list2, exc_list2


def read_uncertainty(f_n_1, f_n_2, exclude=None):
    """
    read in uncertainty and normalize
    """
    u_const_tmp = np.loadtxt(f_n_1, dtype=float, delimiter=' ', comments='#')
    if len(np.shape(u_const_tmp)) == 2:
        u_const = copy.deepcopy(u_const_tmp[:, 1])
    else:
        u_const = copy.deepcopy(u_const_tmp)

    u_random = np.loadtxt(f_n_2, dtype=float, delimiter=',', comments='#')

    # data modification, logendre polynomial is orthonormal in the range of
    # (-1, 1)
    for i, _ in enumerate(u_const):
        # species case when u_const is 1.0
        if u_const[i] == 1.0:
            u_random[:, i] = u_random[:, i] * 0.0
            continue
        u_random[:, i] = (u_random[:, i] - 1 / u_const[i]) \
            / (u_const[i] - 1 / u_const[i])
        u_random[:, i] = u_random[:, i] * 2 - 1
    if exclude is not None:
        include = []
        for i in range(len(u_const)):
            if i not in exclude:
                include.append(i)
        u_random = u_random[:, include]
    return u_random


def read_normalize_uncertainty(f_n_1, f_n_2, exclude=None):
    """
    read and normalize the uncertainties, logendre polynomial requires that data
    should be in the range of (-1, 1), notice the uncertainty range has form of
    (r_idx, min, max)
    """
    # read in uncertainty range, (r_idx, min, max)
    u_range = np.loadtxt(f_n_1, dtype=float, delimiter=',', comments='#')
    if len(np.shape(u_range)) != 2:
        raise ValueError("Number of reaction should be greater than 1, got {0}".format(
            np.shape(u_range)))
    mC, nC = np.shape(u_range)
    if nC != 3:
        raise ValueError(
            "Uncertainty range shall in be the form of (r_idx, min, max)")

    u_random = np.loadtxt(f_n_2, dtype=float, delimiter=',', comments='#')
    if len(np.shape(u_random)) != 2:
        raise ValueError("Number of sample should be greater than 1, got {0}".format(
            np.shape(u_random)))
    _, nR = np.shape(u_random)

    if mC != nR:
        raise ValueError(
            '#reaction of uncertainties range shall be the same as #reaction of'
            'random uncertainties, got {0} vs. {1}'.format(mC, nR))

    # data modification, logendre polynomial is orthonormal in the range of
    # (-1, 1)
    for i, val in enumerate(u_range):
        # species case when u_range is 1.0
        xmin = val[1]
        xmax = val[2]
        if xmin == xmax:
            u_random[:, i] = u_random[:, i] * 0.0
            continue
        assert(xmax > xmin)
        u_random[:, i] = (u_random[:, i] - xmin) \
            / (xmax - xmin)
        u_random[:, i] = u_random[:, i] * 2 - 1
    if exclude is not None:
        include = []
        for i in range(len(u_range)):
            if i not in exclude:
                include.append(i)
        u_random = u_random[:, include]
        u_range = u_range[include, :]

    return u_random, u_range[:, [1, 2]]


def read_uncertainty_const(data_dir, w2f=True, fn="uncertainties.inp"):
    """
    read uncertainties range or all reactions
    """
    f_p = os.path.join(data_dir, "input", fn)
    with open(f_p, 'r') as f_handler:
        lines = f_handler.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    lines = [x.strip() for x in lines]
    keyVmap = {}
    for line in lines:
        key_value = re.split(' |;|,|\n', line)
        if len(key_value) == 2:
            keyVmap[int(key_value[0])] = float(key_value[1])
    # print(keyVmap)
    # print(len(keyVmap))
    u_c_vec = np.ones(len(keyVmap))
    for _, key in enumerate(keyVmap):
        # print(key, keyVmap[key])
        # index starts from 1, convert to 0
        u_c_vec[key - 1] = keyVmap[key]

    if w2f is True:
        # save constant uncertainty to file
        f_n_u_const = os.path.join(
            data_dir, "output", "uncertainties_const.csv")
        np.savetxt(f_n_u_const, u_c_vec, fmt='%.18e',
                   delimiter=',', newline='\n')

    return u_c_vec


def read_uncertainty_random(data_dir, fn="uncertainties_random.csv"):
    """
    read uncertainty random from output/uncertainties_random.csv
    """
    f_p = os.path.join(data_dir, "output", fn)
    u_c_r = np.loadtxt(f_p, dtype=float, delimiter=',')
    u_c_r_value = u_c_r[:, 1]
    return u_c_r_value


def read_target(f_n):
    """
    read in target
    """
    target = np.loadtxt(f_n, dtype=float, delimiter=',', comments='#')
    return target


def prepare_uncertainties_const(data_dir, N=638):
    """
    prepare uncertainties const, prepare reaction_index_old_mechanism.txt and
    reaction_labelling_old_mechanism.csv in advance, index starts with 1,
    "reaction_index_old_mechanism.txt" saves important reactions labelled by the
    old reaction indices system
    """
    fn1 = os.path.join(data_dir, "input", "reaction_index_old_mechanism.txt")
    if not os.path.exists(fn1):
        raise FileNotFoundError(
            "File {0} not found, please configure reaction index of interest \
            from mechanism used in combustion pathway paper.".format(fn1))

    r_idx_old_set = set()
    with open(fn1, 'r') as fh1:
        lines = fh1.readlines()
    for line in lines:
        l_s = re.split(r';\s*|,\s*|\n', line)
        for n in l_s:
            try:
                n_tmp = int(n)
                # index starts with 1 in the old chemkin index representation
                r_idx_old_set.add(abs(n_tmp))
                # the first good number of current line, go next line
                break
            except ValueError:
                pass
    r_idx_old_list = list(sorted(list(r_idx_old_set)))
    print(len(r_idx_old_list))

    fn2_in = "reaction_labelling_old_mechanism.csv"
    fn2 = os.path.join(data_dir, "input", fn2_in)
    if not os.path.exists(fn2):
        raise FileNotFoundError(
            "File {0} not found, please copy reaction_labelling.csv and rename it, the \
            one from mechanism used in combustion pathway paper.".format(fn2))
    r_idx_name_old = psri.parse_chemkin_reaction_index_name(data_dir, fn2_in)
    print(len(r_idx_name_old))

    # new mechanism, old chemkin style index, start with 1
    r_idx_name_new_mechanism = psri.parse_chemkin_reaction_index_name(data_dir)
    print(len(r_idx_name_new_mechanism))
    rxn_name_to_old_chemkin_index = {}
    for k in r_idx_name_new_mechanism:
        rxn_name_to_old_chemkin_index[r_idx_name_new_mechanism[k]] = k
    print(len(rxn_name_to_old_chemkin_index))

    chemkin_rxn_idx_name_d = {}
    for r_idx_old in r_idx_old_list:
        r_name = r_idx_name_old[r_idx_old]
        # print(r_name)
        chemkin_rxn_idx_name_d[rxn_name_to_old_chemkin_index[r_name]] = r_name

    print(len(chemkin_rxn_idx_name_d))
    print(chemkin_rxn_idx_name_d)

    dup_rxn_list = read_duplicated_reactions(data_dir)
    for idx, _ in enumerate(dup_rxn_list):
        # C++ style index to old chemkin index, starts with 1
        dup_rxn_list[idx] += 1
    print(dup_rxn_list)
    dup_rxn_set = set(dup_rxn_list)

    # index start with 1
    inc_set = set()
    exc_set = set()
    for idx in range(1, N+1):
        if idx in chemkin_rxn_idx_name_d and idx not in dup_rxn_set:
            inc_set.add(idx)
        else:
            exc_set.add(idx)
    print(len(inc_set) + len(exc_set))
    return inc_set, exc_set


def update_uncertainties_const_s2f(data_dir, must_include=None, N=638, value=5.0,
                                   inc_at_least_num=1, inc_at_most_num=80):
    """
    update uncertainties_const and save to file, must_include's index starts with 1
    """
    inc_set, exc_set = prepare_uncertainties_const(data_dir, N)
    inc_set_base = copy.deepcopy(inc_set)
    must_include_set = set(must_include)
    # new mechanism, old chemkin style index, start with 1
    chemkin_rxn_idx_name_d = psri.parse_chemkin_reaction_index_name(data_dir)
    print("Input len(inc_set): {0}, len(exc_set): {1}".format(
        len(inc_set), len(exc_set)))
    if must_include is not None and len(must_include) > 0:
        for _, val in enumerate(must_include):
            if val in exc_set and val not in inc_set:
                exc_set.remove(val)
                inc_set.add(val)

    while len(inc_set) < inc_at_least_num:
        r_idx = exc_set.pop()
        # make sure it is a good index
        if r_idx in inc_set_base:
            inc_set.add(r_idx)
        else:
            exc_set.add(r_idx)
    while len(inc_set) > inc_at_most_num and len(inc_set) > 0:
        r_idx = inc_set.pop()
        # make sure it is a good index
        if r_idx not in must_include_set:
            exc_set.add(r_idx)
        else:
            inc_set.add(r_idx)
    print(len(inc_set) + len(exc_set))
    print("Output len(inc_set): {0}, len(exc_set): {1}".format(
        len(inc_set), len(exc_set)))
    print("Output inc_list: {0}".format(list(sorted(copy.deepcopy(inc_set)))))

    fn = "uncertainties_backup.inp"
    with open(os.path.join(data_dir, "input", fn), 'w') as fh:
        for idx in range(1, N+1):
            if idx in inc_set and idx not in exc_set:
                fh.write(str(idx) + ' ' + str(5.0))
            else:
                fh.write(str(idx) + ' ' + str(1.0))
            if idx <= N:
                fh.write("\n")

    fn2 = "uncertainties_rxn_idx_name_backup.csv"
    with open(os.path.join(data_dir, "input", fn2), 'w') as fh2:
        for idx in range(1, N+1):
            if idx in inc_set and idx not in exc_set:
                # C++ style index, starts with 0, not chemkin style index, which starts with 1
                fh2.write(str(idx-1) + ',' +
                          str(chemkin_rxn_idx_name_d[idx]) + '\n')


if __name__ == '__main__':
    INIT_TIME = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', default="default")
    parser.add_argument('-j', '--jobType',
                        default="prepare_uncertainties_const")

    args = parser.parse_args()
    if args.directory == "default":
        DATA_DIR = os.path.abspath(os.path.join(os.path.realpath(
            sys.argv[0]), os.pardir, os.pardir, os.pardir, os.pardir, "SOHR_DATA"))
    else:
        DATA_DIR = args.directory
        # DATA_DIR = r"D:\VS_workspace\CPlusPlus\SOHR\projects\catalytic_cycle\theory" +\
        #     r"\mechanism_thermo\raghu_SR_revised_v1"
    print(DATA_DIR)
    if args.jobType == "prepare_uncertainties_const":
        prepare_uncertainties_const(DATA_DIR)
    elif args.jobType == "update_uncertainties_const_s2f":
        MUST_INCLUDE = [490, 491, 492, 632, 633, 634, 635, 636, 637, 638]
        update_uncertainties_const_s2f(DATA_DIR, must_include=MUST_INCLUDE,
                                       N=638,
                                       value=5.0,
                                       inc_at_least_num=1,
                                       inc_at_most_num=len(MUST_INCLUDE))
